<?php

namespace Drupal\poptin\Controller;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for the Poptin module.
 */
class PoptinController extends ControllerBase {
  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */

  /**
   * Use variable.
   *
   * @var baseUrlT
   */
  protected $baseUrlT;
  /**
   * Use variable.
   *
   * @var tokenGenerator
   */
  protected $tokenGenerator;

  /**
   * Implements __construct().
   */
  public function __construct(CsrfTokenGenerator $csrf_token) {
    global $base_url;
    $this->baseUrlT = $base_url;
    $this->tokenGenerator = $csrf_token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('csrf_token')
    );
  }

  /**
   * Implements check().
   */
  public function check() {
    if (!$this->poptinIsUser()) {
      $regclass = "yes";
    }
    else {
      $regclass = 'no';
    }
    $csrftoken         = $this->poptinGetToken();
    $module_img_dir    = $this->baseUrlT . '/' . drupal_get_path('module', 'poptin') . '/images/';
    $generateloginlink = $this->baseUrlT . '/admin/config/poptin/poptinGeneratePoptinLink?csrftoken=' . $csrftoken;
    $poptin_deactivate = $this->baseUrlT . '/admin/config/poptin/poptinDeactivate';
    return [
      '#theme' => 'main_template',
      '#test_var' => $regclass,
      '#csrftoken' => $csrftoken,
      '#module_img_dir' => $module_img_dir,
      '#base_url' => $this->baseUrlT,
      '#generateloginlink' => $generateloginlink,
      '#deactivate_url' => $poptin_deactivate,
    ];
  }

  /**
   * Implements poptinDeactivate().
   */
  public function poptinDeactivate(Request $request) {
    $token = $request->request->get('csrftoken');
    if (!$this->poptinCheckToken($token)) {
      $tmp_arr = [
        'mssg' => "Invalid CSRF TOKEN",
        'status' => 0,
        'token' => $this->poptinGetToken(),
      ];
      die(Json::encode($tmp_arr));
    }
    \Drupal::database()->truncate('poptin')->execute();
    $tmp_arr = [
      'mssg' => "done",
      'status' => 1,
      'token' => $this->poptinGetToken(),
    ];
    die(Json::encode($tmp_arr));
  }

  /**
   * Implements poptinGeneratePoptinLink().
   */
  public function poptinGeneratePoptinLink(Request $request) {
    $token = $request->query->get('csrftoken');
    if (!$this->poptinCheckToken($token)) {
      $tmp_arr = [
        'mssg' => "Invalid csrf token",
        'status' => 0,
        'token' => $this->poptinGetToken(),
      ];
      die(Json::encode($tmp_arr));
    }
    $api_url     = "https://app.popt.in/api/marketplace/";
    $row_details = $this->poptinFetchRow();
    if (!is_array($row_details)) {
      exit(FALSE);
    }
    else {
      if (isset($row_details['token']) && ($row_details['token'] == '')) {
        return new TrustedRedirectResponse("https://app.popt.in/login");
      }
    }
    $url = $api_url . "auth";
    $dataurl = "token=" . $row_details['token'] . "&user_id=" . $row_details['userid'];
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $dataurl,
      CURLOPT_HTTPHEADER => [
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "postman-token: 16ba048a-499c-06c8-517c-cea2abb11945",
      ],
    ]);
    $response = curl_exec($curl);
    $err      = curl_error($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    }
    else {
      $res = json_decode($response);
      if (!isset($res->login_url)) {
        return new TrustedRedirectResponse("https://app.popt.in/login");
      }
      if ($request->query->get('utm_source')) {
        return new TrustedRedirectResponse($res->login_url . "&utm_source=drupal8");
      }
      else {
        return new TrustedRedirectResponse($res->login_url);
      }
    }
  }

  /**
   * Implements poptinSignup().
   */
  public function poptinSignup(Request $request) {
    $token = $request->request->get('csrftoken');
    if (!$this->poptinCheckToken($token)) {
      $tmp_arr = [
        'mssg' => "Invalid csrf token",
        'status' => 0,
        'token' => $this->poptinGetToken(),
      ];
      die(Json::encode($tmp_arr));
    }
    $email_id = $request->request->get('email');
    $api_url  = "https://app.popt.in/api/marketplace/";
    $url      = $api_url . "register";
    $dataurl  = "email=" . $email_id . "&marketplace=drupal8";
    $curl     = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $dataurl,
      CURLOPT_HTTPHEADER => [
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "postman-token: 16ba048a-499c-06c8-517c-cea2abb11945",
      ],
    ]);
    $response = curl_exec($curl);
    $err      = curl_error($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    }
    else {
      $result = json_decode($response);
      if (!empty($result) && $result->success == 1) {
        $tmp_arr = [
          'user_id' => $result->user_id,
          'client_id' => $result->client_id,
          'token' => $result->token,
        ];
        \Drupal::database()->insert('poptin')->fields($tmp_arr)->execute();
        $return_mssg = [
          'mssg' => "User Registered successfully",
          'status' => 1,
          'token' => $this->poptinGetToken(),
        ];
      }
      else {
        $return_mssg = [
          'mssg' => $result->message,
          'status' => 0,
          'token' => $this->poptinGetToken(),
        ];
      }
    }
    echo Json::encode($return_mssg);
    die;
  }

  /**
   * Implements poptinLogin().
   */
  public function poptinLogin(Request $request) {
    $userid = $request->request->get('userid');
    \Drupal::database()->insert('poptin')->fields(['client_id' => $userid])->execute();
    $return_mssg = [
      'mssg' => "User id Registered successfully",
      'status' => 1,
      'token' => $this->poptinGetToken(),
    ];
    die(Json::encode($return_mssg));
  }

  /**
   * Implements poptinFetchRow().
   */
  public function poptinFetchRow() {
    $results                = \Drupal::database()->select('poptin', 'p')->fields('p')->execute();
    $results->allowRowCount = TRUE;
    $num_of_results         = $results->rowCount();
    if ($num_of_results > 0) {
      foreach ($results as $result) {
        return [
          'userid' => $result->user_id,
          'client_id' => $result->client_id,
          'token' => $result->token,
        ];
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Implements poptinIsUser().
   */
  public function poptinIsUser() {
    $result                = \Drupal::database()->select('poptin', 'p')->fields('p')->execute();
    $result->allowRowCount = TRUE;
    $num_of_results        = $result->rowCount();
    if ($num_of_results > 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Implements poptin_check_csrf_token().
   */
  private function poptinCheckToken($token) {
    if (!empty($token)) {
      $testtoken = $this->tokenGenerator->validate($token);
      if ($testtoken) {
        return TRUE;
      }
      return FALSE;
    }
    return FALSE;
  }

  /**
   * Implements poptin_check_csrf_token().
   */
  private function poptinGetToken() {
    $token = $this->tokenGenerator->get();
    return $token;
  }

}
