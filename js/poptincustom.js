jQuery('document').ready(function (e) {
  'use strict';
  jQuery('.pplogout1').on('click', function (e) {
    e.preventDefault();
    var generate_login_url = jQuery('#deactivate_url').html();
    show_loader();
    jQuery.ajax({url: generate_login_url, data: {csrftoken: csrf}, type: 'POST', success: function (response) {
      hide_loader();
      jQuery('#deactivate_poptin_popup').addClass('hidediv').removeClass('open');
      jQuery('.byebyeModal').removeClass('hidediv').addClass('open');
    }});
  });
  jQuery('.close_bybye').on('click', function (e) {
    jQuery('.byebyeModal').addClass('hidediv').removeClass('open');
    jQuery('.poptin_registration').removeClass('hidediv');
    jQuery('.poptin_logged').addClass('hidediv');
    removebacklayer();
  });
  jQuery('.close_where').click(function (e) {
    jQuery('.whereis_myid').addClass('hidediv').removeClass('open');
    removebacklayer();
  });
  jQuery('.close_lookfamiliar').click(function (e) {
    jQuery('.lookfamiliar').addClass('hidediv').removeClass('open');
    removebacklayer();
  });
  jQuery('.close_oopsiewrongid').click(function (e) {
    jQuery('.oopsiewrongid').addClass('hidediv').removeClass('open');
    removebacklayer();
  });
  jQuery('.close_oopsiewrongemailid').click(function (e) {
    jQuery('.oopsiewrongemailid').addClass('hidediv').removeClass('open');
    removebacklayer();
  });
  jQuery('.wheremyid').click(function (e) {
    jQuery('.whereis_myid').removeClass('hidediv').addClass('open');
    jQuery('.oopsiewrongid').addClass('hidediv').removeClass('open');
    addbacklayer();
  });
  jQuery('.pplogin').click(function (e) {
    e.preventDefault();
    var login_url = jQuery('#baseurl').val();
    var client_id = jQuery('#pop_up_id').val();
    if (client_id === '' || client_id.length !== 13) {
      jQuery('.oopsiewrongid').removeClass('hidediv').addClass('open');
      addbacklayer();
      return;
    }
    login_url += '/admin/config/poptin/poptin_login';
    show_loader();
    jQuery.ajax({url: login_url, type: 'POST', data: {userid: client_id, csrftoken: csrf}, success: function (response) {
      hide_loader();
      var jsonres = JSON.parse(response);
      if (jsonres.status === 1) {
        jQuery('.poptin_registration').addClass('hidediv');
        jQuery('.poptin_logged').removeClass('hidediv');
      }
      else {
        var error_s = 'Someting Goes Wrong';
        swal('Error', error_s, 'error');
      }
    },
      error: function (ts) {
        hide_loader();
        swal('Error', ts.responseText, 'error');
      }});
  });
  jQuery('.ppsubmit').click(function (e) {
    e.preventDefault();
    var register_url = jQuery('#baseurl').val();
    var email_id = jQuery('#popin_email').val();
    if (email_id === '') {
      jQuery('.oopsiewrongemailid').removeClass('hidediv').addClass('open');
      addbacklayer();
      return;
    }
    register_url += '/admin/config/poptin/poptin_signup';
    show_loader();
    jQuery.ajax({url: register_url, type: 'POST', data: {email: email_id, csrftoken: csrf}, success: function (result) {
      hide_loader();
      var jsonres = JSON.parse(result);
      if (jsonres.status === 1) {
        jQuery('.poptin_registration').addClass('hidediv');
        jQuery('.poptin_logged').removeClass('hidediv');
        jQuery('.ppcontrolpanel.with_token').attr('href', jQuery('.ppcontrolpanel.with_token').attr('href') + '&utm_source=drupal8');
      }
      else {
        swal('Error', jsonres.mssg, 'error');
      }
    },
      error: function (ts) {
        hide_loader();
        swal('Error', ts.responseText, 'error');
      }});
  });
  function show_loader() {
    jQuery('.poptin-overlay').attr('style');
    jQuery('.poptin-overlay').css('display', 'block');
    jQuery('.poptin-overlay').fadeIn(500);
  }
  function hide_loader() {
    jQuery('.poptin-overlay').attr('style');
    jQuery('.poptin-overlay').fadeOut(500);
    jQuery('.poptin-overlay').css('display', 'none');
  }
  jQuery('#login_form').click(function (event) {
    event.preventDefault();
    jQuery('.login_form').attr('style', ' ');
    jQuery('.register_form').attr('style', ' ');
    jQuery('.login_form').css('display', 'block');
    jQuery('.register_form').css('display', 'none');
  });
  jQuery('.pplogout').click(function (event) {
    jQuery('#deactivate_poptin_popup').removeClass('hidediv').addClass('open').show();
    addbacklayer();
  });
  jQuery('.close_confirm').click(function (event) {
    jQuery('#deactivate_poptin_popup').addClass('hidediv').removeClass('open');
    removebacklayer();
  });
  jQuery('#register_form').click(function (event) {
    event.preventDefault();
    jQuery('.register_form').attr('style', ' ');
    jQuery('.login_form').attr('style', ' ');
    jQuery('.register_form').css('display', 'block');
    jQuery('.login_form').css('display', 'none');
  });
  function addbacklayer() {
    jQuery('body').append('<div class="modal-backdrop fade in"></div>');
  }
  function removebacklayer() {
    jQuery('.modal-backdrop').remove();
  }
  function swal(Error, msg, error) {
    jQuery('.swal_d_display').css('display', 'block');
    jQuery('.swal-text').html(msg);
  }
  jQuery('.swal-button').click(function () {
    jQuery('.swal_d_display').css('display', 'none');
  });
});
