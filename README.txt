CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Customization
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module allows you to manage your poptin.com account within
your Drupal site

For a full description of the project visit the project page:
http://drupal.org/project/poptin

To submit bug reports and feature suggestions please email us at:
contact@poptin.com

REQUIREMENTS
------------

No special requirements, you can create a free poptin.com using
the module (or you can connect your current account)

INSTALLATION
------------

Install as you would normally install a contributed Drupal. See:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
-------------

After installing the Poptin module, you will have two options:
1. Signup and create a new Poptin account through the module. After
filling out your email address a Poptin account will be created for
you and the Poptin JS snippet will be automatically embedded to your site

2. Connect your existing account - if you already have a Poptin account
you can enter your user ID. This way every poptin you create at your
Poptin account will be shown on your website.

TROUBLESHOOTING
-------------

Visit https://help.poptin.com/ for any issue

MAINTAINERS
-----------

Current maintainers:
 * Poptin (poptin) - https://www.drupal.org/user/3609856
